#!/usr/bin/python
import gitlab
from datetime import datetime
gl = gitlab.Gitlab('https://gitlab.com', private_token='xxxxxxxxxxxxxxxx')
project = gl.projects.get(23)
mrs = project.mergerequests.list(state='opened')
for mr in mrs:
#       print mr
        print "---"
        print "[[",mr.iid,"]]",mr.title,"[[",mr.author['name'], mr.created_at,"]]"
        commits = mr.commits()
        last_commit = ""
        last_datetime = datetime.strptime("2010/01/01 00:00", "%Y/%m/%d %H:%M")
        for commit in commits:
                if (datetime.strptime(commit.created_at, "%Y-%m-%dT%H:%M:%S.%fZ") > last_datetime):
                        last_commit = commit.title
                        last_datetime = datetime.strptime(commit.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        print "LAST COMMIT: ", last_commit, last_datetime
        elapsedTime = last_datetime - datetime.strptime(mr.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        minutes,seconds = divmod(elapsedTime.total_seconds(), 60)
        hours,minutes = divmod(minutes, 60)
        print "OPEN/LAST_COMMIT DIFF: ", int(hours), "hours", int(minutes), "minutes"
        elapsedTime = datetime.utcnow() - last_datetime
        minutes,seconds = divmod(elapsedTime.total_seconds(), 60)
        hours,minutes = divmod(minutes, 60)
        print "LAST_COMMIT/NOW DIFF: ", int(hours), "hours", int(minutes), "minutes"